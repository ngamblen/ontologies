Here are the ontologies which development I initiated.

# Systems Biology Ontology (SBO)

The Systems Biology Ontology is a set of controlled, relational vocabularies of terms commonly used in Systems Biology, and in particular in computational modeling. The ontology consists of six orthogonal vocabularies defining: the roles of reaction participants (eg. “substrate”), quantitative parameters (eg. “Michaelis constant”), a precise classification of mathematical expressions that describe the system (eg. “mass action rate law”), the modeling framework used (eg. “logical framework”), and a branch each to describe entity (eg. “macromolecule”) and interaction (eg. “process”) types. SBO terms can be used to introduce a layer of semantic information into the standard description of a model, or to annotate the results of biochemical experiments in order to facilitate their efficient reuse. SBO is an Open Biomedical Ontologies (OBO) candidate ontology, and is free for use. SBO is a project of the BioModels.net effort and is developed through community collaboration.

## Where to find it?

https://github.com/EBI-BioModels/SBO

https://www.ebi.ac.uk/ols4/ontologies/sbo

https://bioportal.bioontology.org/ontologies/SBO

## Cite

SBO was initially presented initially in:

Le Novère N. BioModels.net, tools and resources to support Computational Systems Biology. _Proceedings of the 4th Workshop on Computation of Biochemical Pathways and Genetic Networks_ (2005), Logos, Berlin, pp. 69-74. ISBN-13:978-3832510183 [link](https://ascistance.co.uk/perso/gambardella/PUBLIS/Lenov2005b.pdf)

and in:

Le Novère N., Courtot M., Laibe C. Adding semantics in kinetics models of biochemical pathways. _Proceedings of the 2nd International Symposium on experimental standard conditions of enzyme characterizations_ (2007), 137-153. [link](https://ascistance.co.uk/perso/gambardella/PUBLIS/Lenov2007b.pdf)

But the main reference is:

Courtot M., Juty N., Knüpfer C., Waltemath D., Zhukova A., Dräger A., Dumontier M., Finney A., Golebiewski M., Hastings J., Hoops S., Keating S., Kell D.B., Kerrien S., Lawson J., Lister A., Lu J., Machne R., Mendes P., Pocock M., Rodriguez N., Villeger A., Wilkinson D.J., Wimalaratne S., Laibe C., Hucka M., Le Novère N. Controlled vocabularies and semantics in Systems Biology. _Molecular Systems Biology_ (2011), 7: 543. [doi:10.1038/msb.2011.77](https://doi.org/10.1038/msb.2011.77)

# Kinetic Simulation Algorithm Ontology (KiSA0)

The Kinetic Simulation Algorithm Ontology (KiSAO) is an ontology of algorithms for simulating and analyzing biological models, as well as the characteristics of these algorithms, their input parameters, and their outputs. In addition, KiSAO captures relationships among algorithms, their parameters, and their outputs.

## Where to find it?

https://www.ebi.ac.uk/ols4/ontologies/kisao

https://bioportal.bioontology.org/ontologies/KISAO

https://github.com/SED-ML/KiSAO

## Cite

SBO was initially presented initially in:

Köhn D.,Le Novère N. SED-ML - An XML Format for the Implementation of the MIASE Guidelines. Proceedings of the 6th conference on Computational Methods in Systems Biology (2008), Heiner M and Uhrmacher AM eds, _Lecture Notes in Bioinformatics_, 5307: 176-190. [doi:10.1007/978-3-540-88562-7](https://doi.org/10.1007/978-3-540-88562-7)

But the main reference is:

Courtot M., Juty N., Knüpfer C., Waltemath D., Zhukova A., Dräger A., Dumontier M., Finney A., Golebiewski M., Hastings J., Hoops S., Keating S., Kell D.B., Kerrien S., Lawson J., Lister A., Lu J., Machne R., Mendes P., Pocock M., Rodriguez N., Villeger A., Wilkinson D.J., Wimalaratne S., Laibe C., Hucka M., Le Novère N. Controlled vocabularies and semantics in Systems Biology. _Molecular Systems Biology_ (2011), 7: 543. [doi:10.1038/msb.2011.77](https://doi.org/10.1038/msb.2011.77)

## libKiSAO

https://sourceforge.net/projects/kisao/files/

Zhukova A., Adams R., Laibe C., Le Novère N. LibKiSAO: a Java Library for Querying KiSAO. _BMC Research Notes_ (2012), 5: 520. [doi:10.1186/1756-0500-5-520](https://doi.org/10.1186/1756-0500-5-520)

# Mathematical Modelling Ontology (MAMO)

The Mathematical Modelling Ontology (MAMO) is a classification of the types of mathematical models used mostly in the life sciences, their variables, relationships and other relevant features.

## Where

https://bioportal.bioontology.org/ontologies/MAMO

## Cite

# Terminology for the Description of Dynamics (TEDDY)

The TErminology for the Description of DYnamics (TEDDY) project aims to provide an ontology for dynamical behaviours, observable dynamical phenomena, and control elements of bio-models and biological systems in Systems Biology and Synthetic Biology.

## Where

https://www.ebi.ac.uk/ols4/ontologies/teddy

## Cite

Courtot M., Juty N., Knüpfer C., Waltemath D., Zhukova A., Dräger A., Dumontier M., Finney A., Golebiewski M., Hastings J., Hoops S., Keating S., Kell D.B., Kerrien S., Lawson J., Lister A., Lu J., Machne R., Mendes P., Pocock M., Rodriguez N., Villeger A., Wilkinson D.J., Wimalaratne S., Laibe C., Hucka M., Le Novère N. Controlled vocabularies and semantics in Systems Biology. _Molecular Systems Biology_ (2011), 7: 543. [doi:10.1038/msb.2011.77](https://doi.org/10.1038/msb.2011.77)

